package com.gitlab.kamiltrzcinski1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

public class GuessGame {

    private static GuessGame instance = new GuessGame();
    private int score;
    private Random random = new Random();
    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    private GuessGame() {

    }

    public static GuessGame getInstance() {
        return instance;
    }

    public void play() {
        for (int i = 0; i < 10; i++) {
            System.out.println("type number 0-9: ");
            int unknown = random.nextInt(10);
            int guessing = 0;
            try {
                guessing = Integer.parseInt(reader.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (guessing == unknown) {
                score++;
                System.out.println("GUESS!");
            } else {
                System.out.println("not guess!");
            }
        }
    }

    protected Object readResolve() {
        return getInstance();
    }

    public int getScore() {
        return score;
    }
}
