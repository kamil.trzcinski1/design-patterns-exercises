package com.gitlab.kamiltrzcinski1.weather;

import com.gitlab.kamiltrzcinski1.notification.Observer;

public interface Observable {
    void registerObserver(Observer observer);
    void unregisterObserver(Observer observer);
    void notifyObservers();
}
