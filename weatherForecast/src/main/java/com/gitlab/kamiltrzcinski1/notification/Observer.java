package com.gitlab.kamiltrzcinski1.notification;

import com.gitlab.kamiltrzcinski1.weather.WeatherForecast;

public interface Observer {
    void updateForecast(WeatherForecast weatherForecast);
}
