package com.gitlab.kamiltrzcinski1;

import com.gitlab.kamiltrzcinski1.notification.InternetNews;
import com.gitlab.kamiltrzcinski1.notification.RadioNews;
import com.gitlab.kamiltrzcinski1.notification.TvNews;
import com.gitlab.kamiltrzcinski1.weather.WeatherForecast;

public class App 
{
    public static void main( String[] args )
    {
        WeatherForecast weatherForecast = new WeatherForecast(25, 1003);
        RadioNews radioNews = new RadioNews();
        InternetNews internetNews = new InternetNews();
        TvNews tvNews = new TvNews();
        weatherForecast.registerObserver(radioNews);
        weatherForecast.registerObserver(internetNews);
        weatherForecast.registerObserver(tvNews);
        weatherForecast.notifyObservers();
        weatherForecast.unregisterObserver(tvNews);
        weatherForecast.unregisterObserver(radioNews);
        System.out.println("Nowa prognoza - powiadomienie tylko dla serwisu internetowego:");
        weatherForecast.updateForecast(18, 1007);

    }
}
